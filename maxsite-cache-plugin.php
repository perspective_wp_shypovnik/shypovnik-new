<?php
/*
Plugin Name: Maxsite MAXCACHE
Plugin URI: http://maxsite.org/page/maxsite-cache
Description: Учет залогиненности кеша MAXCACHE
Author: MAX
Version: 3.0
Author URI: http://maxsite.org/
*/

if (file_exists(ABSPATH . 'maxsite-cache.php'))
{
	require_once(ABSPATH . 'maxsite-cache.php');
	
	add_action('wp_login', 'maxsite_cache_login');
	add_action('wp_logout', 'maxsite_cache_logout');
	
}

?>