<?php

# время кэша в секундах
define('MSO_TIME', 4 * 60 * 60); // 4 часа


# ссылка для сброса кэша (http://сайт/ваша_ссылка)
define('MSO_CACHE_FLUSH', 'cache-flush');


# статистика - выберите/исправьте нужный формат
//define('MSO_STAT', '<p style="text-align: center">MAXCACHE: [MSO_MEMORY]/[MSO_TIME]</p>'); # - без ссылки
//define('MSO_STAT', '<p style="text-align: center"><a href="http://maxsite.org/">MAXCACHE</a>: [MSO_MEMORY]/[MSO_TIME]</p>');
// define('MSO_STAT', 'MAXCACHE: [MSO_MEMORY]/[MSO_TIME]'); # - только статистика
 define('MSO_STAT', '<!-- MAXCACHE: [MSO_MEMORY]/[MSO_TIME] -->'); # - в комментарии не видна на сайте
// define('MSO_STAT', ''); # - вообще пусто


# если нужно использовать список некешируемых адресов, то укажите имя файла
# если не нужно, то оставьте его равным false
# пример задания адресов см. в файле maxsite-cache-no-cache.php
define('MSO_URI_NO_CACHE_FILE', false); # не использовать список
//define('MSO_URI_NO_CACHE_FILE', 'maxsite-cache-no-cache.php'); # список в maxsite-cache-no-cache.php


# если вы хотите отслеживать статистику попаданий кэша, то укажите
# в этом параметре имя файла. Для него нужно установить права на запись, обычно 666
# Обратите внимание, что размер файла может сильно увеличиться, поэтому
# включайте статистику только на интересующий вас период, например одни сутки
define('MSO_STASTSTIC_FILE', false); # не использовать статистику
//define('MSO_STASTSTIC_FILE', 'maxsite-cache-statistic.txt'); 


# время жизни куки при включении залогированности
define('MSO_COOKIE_TIME', 60 * 60 * 24); # 24 часа

# включить сжатие трафика с помощью zlib
# на некоторых хостингах может не работать!
//define('MSO_ZLIB_OUTPUT', false); // выключить
define('MSO_ZLIB_OUTPUT', true); // включить

