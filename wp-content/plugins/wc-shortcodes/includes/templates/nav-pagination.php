<?php
// Don't print empty markup if there's only one page.
if ( $wc_shortcodes_posts_query->max_num_pages < 2 )
	return;
?>
<nav class="navigation paging-navigation" role="navigation" align="center">
	<h6 class="screen-reader-text"><?php _e( '', 'wordpresscanvas' ); ?></h6>
	<div class="nav-links">
		<?php 
			$big = 999999999; // need an unlikely integer
			$args = array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?page=%#%', // ?page=%#% : %#% is replaced by the page number
				'total' => $wc_shortcodes_posts_query->max_num_pages,
				'current' => max( 1, $wc_shortcodes_posts_query->get('paged') ),
				'show_all' => false,
				'prev_next' => true,
				'prev_text' => __('|<'),
				'next_text' => __('>|'),
				'end_size' => 2,
				'mid_size' => 2,
				'type' => 'plain',
				'add_args' => false, // array of query args to add
				'add_fragment' => ''
			);
		?>
		<?php echo paginate_links( $args ); ?>

	</div><!-- .nav-links -->
</nav><!-- .navigation -->
