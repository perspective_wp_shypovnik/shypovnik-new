<?php // Theme Options vars

	$media_filter         = of_get_option('media_filter');

	$category_value       = get_post_meta($post->ID, 'tz_category_include', true);

	$media_filter_orderby = ( of_get_option('media_filter_orderby') ) ? of_get_option('media_filter_orderby') : 'name';

	$media_filter_order   = ( of_get_option('media_filter_order') ) ? of_get_option('media_filter_order') : 'name';



	// WPML filter

	$suppress_filters = get_option('suppress_filters');

?>

<div class="page_content">

	<?php the_content(); ?>

	<div class="clear"></div>

</div>

<?php

if ( post_password_required() ) {

	return;

}

if ( !$category_value ) {

	switch ($media_filter) {

		case 'cat': ?>

			<div class="filter-wrapper clearfix">

				<div class="pull-right">

					<strong><?php echo theme_locals("categories"); ?>: </strong>

					<ul id="filters" class="filter nav nav-pills">

						<?php

							// query

							$args = array(

								'post_type'        => 'post',

								'posts_per_page'   => -1,

								'post_status'      => 'publish',

								'orderby'          => 'name',

								'order'            => 'ASC',

								'suppress_filters' => $suppress_filters

								);

							$media_posts = get_posts($args);



							foreach( $media_posts as $k => $media ) {

								//Check if WPML is activated

								if ( defined( 'ICL_SITEPRESS_VERSION' ) ) {

									global $sitepress;



									$post_lang = $sitepress->get_language_for_element($media->ID, 'post_media');

									$curr_lang = $sitepress->get_current_language();

									// Unset not translated posts

									if ( $post_lang != $curr_lang ) {

										unset( $media_posts[$k] );

									}

									// Post ID is different in a second language Solution

									if ( function_exists( 'icl_object_id' ) ) {

										$media = get_post( icl_object_id( $media->ID, 'post', true ) );

									}

								}

							}

							$count_posts = count($media_posts);

						?>

						<li class="active"><a href="#" data-count="<?php echo $count_posts; ?>" data-filter><?php echo theme_locals("show_all"); ?></a></li>

						<?php

							$filter_array = array();

							$media_categories = get_categories( array(

								'taxonomy' => 'media_category',

								'orderby'  => $media_filter_orderby,

								'order'    => $media_filter_order,

								)

							);

							foreach($media_categories as $media_category) {

								$filter_array[$media_category->name] = $media_category->count;

							}



							if ($paged == 0) $paged = 1;

							$custom_count = ($paged - 1) * $items_count;



							// query

							$args = array(

								'post_type'        => 'post',

								'showposts'        => $items_count,

								'offset'           => $custom_count,

								'suppress_filters' => $suppress_filters,

								);

							$the_query = new WP_Query($args);



							while( $the_query->have_posts() ) :

								$the_query->the_post();

								$post_id = $the_query->post->ID;

								$terms = get_the_terms( $post_id, 'media_category');

								if ( $terms && ! is_wp_error( $terms ) ) {

									foreach ( $terms as $term ) {

										$filter_array[$term->name] = $term;

									}

								}

							endwhile;



							foreach ($filter_array as $key => $value) {

								if ( isset($value->count) ) {

									echo '<li><a href="#" data-count="'. $value->count .'" data-filter=".term_id_'.$value->term_id.'">' . $value->name . '</a></li>';

								}

							}

							wp_reset_postdata();

						?>

					</ul>

					<div class="clear"></div>

				</div>

			</div>

			<?php

			break;

		case 'tag': ?>

			<div class="filter-wrapper clearfix">

				<div class="pull-right">

					<strong><?php echo theme_locals("tags"); ?>: </strong>

					<ul id="tags" class="filter nav nav-pills">

						<?php

							// query

							$args = array(

								'post_type'        => 'post',

								'posts_per_page'   => -1,

								'post_status'      => 'publish',

								'orderby'          => 'name',

								'order'            => 'ASC',

								'suppress_filters' => $suppress_filters

								);

							$media_posts = get_posts($args);



							foreach( $media_posts as $k => $media ) {

								// Unset not translated posts

								if ( function_exists( 'wpml_get_language_information' ) ) {

									global $sitepress;



									$check               = wpml_get_language_information( $media->ID );

									$language_code = substr( $check['locale'], 0, 2 );

									if ( $language_code != $sitepress->get_current_language() ) unset( $media_posts[$k] );



									// Post ID is different in a second language Solution

									if ( function_exists( 'icl_object_id' ) ) $media = get_post( icl_object_id( $media->ID, 'post', true ) );

								}

							}

							$count_posts = count($media_posts);

						?>

						<li class="active"><a href="#" data-count="<?php echo $count_posts; ?>" data-filter><?php echo theme_locals("show_all"); ?></a></li>

						<?php

							$filter_array = array();

							$media_tags = get_terms( 'media_tag', array(

								'orderby'  => $media_filter_orderby,

								'order'    => $media_filter_order,

								)

							);

							foreach($media_tags as $media_tag) {

								$filter_array[$media_tag->slug] = $media_tag->count;

							}



							if ($paged == 0) $paged = 1;

							$custom_count = ($paged - 1) * $items_count;



							// query

							$args = array(

								'post_type'        => 'post',

								'showposts'        => $items_count,

								'offset'           => $custom_count,

								'suppress_filters' => $suppress_filters

								);

							$the_query = new WP_Query($args);



							while( $the_query->have_posts() ) :

								$the_query->the_post();

								$post_id = $the_query->post->ID;

								$terms = get_the_terms( $post_id, 'media_tag');

								if ( $terms && ! is_wp_error( $terms ) ) {

									foreach ( $terms as $term ) {

										$filter_array[$term->slug] = $term;

									}

								}

							endwhile;



							foreach ($filter_array as $key => $value) {

								if ( isset($value->count) ) {

									echo '<li><a href="#" data-count="'. $value->count .'" data-filter=".term_id_'.$value->term_id.'">' . $value->name . '</a></li>';

								}

							}

							wp_reset_postdata();

						?>

					</ul>

					<div class="clear"></div>

				</div>

			</div>

			<?php

			break;

		default:

			break;

	}

}?>



<?php

	// http://codex.wordpress.org/Pagination#Adding_the_.22paged.22_parameter_to_a_query

	if ( get_query_var('paged') ) {

		$paged = get_query_var('paged');

	} elseif ( get_query_var('page') ) {

		$paged = get_query_var('page');

	} else {

		$paged = 1;

	}



	// Get Order & Orderby Parameters

	$orderby = ( of_get_option('media_posts_orderby') ) ? of_get_option('media_posts_orderby') : 'date';

	$order   = ( of_get_option('media_posts_order') ) ? of_get_option('media_posts_order') : 'DESC';



	// The Query

	$args = array(

		'post_type'          => 'post',

		'paged'              => $paged,

		'showposts'          => $items_count,

		'media_category' => $category_value,

		'suppress_filters'   => $suppress_filters,

		'orderby'            => $orderby,

		'order'              => $order,

		);

	global $query_string;

	query_posts($args);

?>



<?php if ( !have_posts() ) : ?>

	<div id="post-0" class="post error404 not-found">

		<h1 class="entry-title"><?php echo theme_locals("not_found"); ?></h1>

		<div class="entry-content">

			<p><?php echo theme_locals("apologies"); ?></p>

			<?php get_search_form(); ?>

		</div><!-- .entry-content -->

	</div><!-- #post-0 -->

<?php endif; ?>



<ul id="media-grid" class="filterable-media thumbnails media-<?php echo $cols; ?>" data-cols="<?php echo $cols; ?>">

	<?php get_template_part('filterable-media-loop'); ?>

</ul>



<?php

	get_template_part('includes/post-formats/post-nav');

	wp_reset_query();

?>