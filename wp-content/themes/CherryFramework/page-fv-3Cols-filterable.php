<?php
/**
* Template Name: Photo Video 3 cols
*/

get_header(); ?>

<div class="motopress-wrapper content-holder clearfix">
	<div class="container">
		<div class="row">
			<div class="<?php echo cherry_get_layout_class( 'full_width_content' ); ?>" data-motopress-wrapper-file="page-fv-3Col-filterable.php" data-motopress-wrapper-type="content">
				<div class="row">
					<div class="<?php echo cherry_get_layout_class( 'full_width_content' ); ?>" data-motopress-type="static" data-motopress-static-file="static/static-title.php">
						<?php get_template_part("static/static-title"); ?>
						<div id="fv_filter">
							Фильтр :
							<select name="post_filter" id="post_filter">
								<option <?=!isset($_GET['type'])||!$_GET['type']||$_GET['type']=='all'?'selected':''?> value="all">Все</option>
								<option <?=isset($_GET['type'])&&$_GET['type']=='post-format-image'?'selected':''?> value="post-format-image">Изображения</option>
								<option <?=isset($_GET['type'])&&$_GET['type']=='post-format-gallery'?'selected':''?> value="post-format-gallery">Галлерея</option>
								<option <?=isset($_GET['type'])&&$_GET['type']=='post-format-audio'?'selected':''?> value="post-format-audio">Аудио</option>
								<option <?=isset($_GET['type'])&&$_GET['type']=='post-format-video'?'selected':''?> value="post-format-video">Видео</option>
							</select>
							<script>
								$('#post_filter').change(function(){
									location.href = '?type='+$(this).val();
								});
							</script>
						</div>
					</div>
				</div>
				<div id="content" class="row">
					<div id="foto-video"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>