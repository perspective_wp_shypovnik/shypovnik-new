<?php /* Static Name: Navigation */ ?>
<!-- BEGIN MAIN NAVIGATION -->
<nav class="nav nav__primary clearfix">
<?php if (has_nav_menu('header_menu')) {
	wp_nav_menu( array(
		'container'      => 'ul',
		'menu_class'     => 'sf-menu',
		'menu_id'        => 'topnav',
		'depth'          => 0,
		'theme_location' => 'header_menu',
		'walker'         => new description_walker()
	));

?>
	<ul class="social_menu">
		<li><a href="https://www.facebook.com/shipovnik.ua"><img src="<?=get_stylesheet_directory_uri()?>/images/icon_facebook.png" /></a></li>
		<li><a href="https://twitter.com/Shipovnik_ua"><img src="<?=get_stylesheet_directory_uri()?>/images/icon_twitter.png" /></a></li>
		<li><a href="https://vk.com/shipovnik_ua"><img src="<?=get_stylesheet_directory_uri()?>/images/icon_vk.png" /></a></li>
		<li><a href="https://www.instagram.com/shipovnik.ua/"><img src="<?=get_stylesheet_directory_uri()?>/images/icon_instagram.png" /></a></li>
	</ul>
	<script>
		$(document).ready(function(){
			function menu_social() {
				var menu = $('#mega_main_menu_ul').offset();
				var li = $('#mega_main_menu_ul li:nth-last-child(2)').offset();
				var pos = li.left - menu.left;

				if($(window).width() > 980){
					$('ul.social_menu').css('left', pos+150+'px').show();
				} else {
					$('ul.social_menu').css('left', pos+112+'px').show();
				}

			}

			$(window).resize(function(){
				if($(window).width() > 768){
					menu_social();
				}
			});

			menu_social();
		});
	</script>
<?php
} else {
	echo '<ul class="sf-menu">';
		$ex_page = get_page_by_title( 'Privacy Policy' );
		if ($ex_page === NULL) {
			$ex_page_id = '';
		} else {
			$ex_page_id = $ex_page->ID;
		}
		wp_list_pages( array(
			'depth'    => 0,
			'title_li' => '',
			'exclude'  => $ex_page_id
			)
		);

	echo '</ul>';} ?>		
</nav><!-- END MAIN NAVIGATION -->