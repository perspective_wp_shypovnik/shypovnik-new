<?php /* Wrapper Name: Header */ ?>



<div class="block_1">
	<div class="row">

		<div class="span12" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="header-sidebar-1">
			<div class="header_widget_1" align="center"></div>
		</div>
	</div>
</div>

<div class="row">

	<div class="span3" data-motopress-type="static" data-motopress-static-file="static/static-logo.php">
		<?php get_template_part("static/static-logo"); ?>
	</div>
		<div class="span9">
		<div class="wrapper">
			<ins data-revive-zoneid="1" data-revive-id="f45a603f367bc4040697e324316cf205"></ins>
			<script async src="//ads.shipovnik.ua/www/delivery/asyncjs.php"></script>
			<div class="header_widget_2" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="header-sidebar-2">
				<?php dynamic_sidebar("header-sidebar-2"); ?>
			</div>

		</div>
	</div>

</div>

<div class="block_2">
	<div class="row">

		<div class="<?php echo cherry_get_layout_class( 'full_width_content' ); ?>" data-motopress-type="static" data-motopress-static-file="static/static-nav.php">
			<?php get_template_part("static/static-nav"); ?> 	
		</div>			

	</div>
</div>